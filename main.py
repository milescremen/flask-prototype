#export FLASK_APP=main.py
#flask run

from flask import Flask, render_template

import time
import os
import subprocess

app = Flask(__name__)

def spawn_process():
    print("Entered spawn process")
    output_string = "eeee"
    f = open("output.txt", "w")
    
    proc = subprocess.Popen(["sudo", "mafft", "test_input.txt"], stdout=f)
    proc.wait()
    f.close()

    f = open("output.txt", "r")
    output_string = f.read()
    f.close()

    return(output_string)

@app.route('/')
def home_page():
    output_string = spawn_process()
    return(render_template('index.html', mafft_out=output_string))
